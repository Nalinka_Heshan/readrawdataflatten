package com;

import com.connection.Mongo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.plan.PlanDataHandler;
import com.plan.map_plan.Mapping;
import com.plan.map_plan.Plan;
import com.plan.read.PlanMapping;
import com.read_raw_data.GenerateSubjects;
import com.read_raw_data.ReadEmProData;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class Main {
    static String filePath;
    public static void main(String[] args) throws JsonProcessingException {

        String user ="ind";
        String source="plan_upload";
        String subjectKey="ind-pemp-p122";
        String planName="production_plan";
        //String planName="finishing_plan";
        filePath="/home/heshan/ncinga/TracerRawDataRead/src/main/java/com/read_raw_data/raw_input_data.json";

        PlanDataHandler planData= new PlanDataHandler(Mongo.getInstance());
        Plan plan =planData.insertPlan(filePath,planName,user,source,subjectKey);
        Document d1= plan.getPlanRecords().get(20);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonData = mapper.readTree(d1.toJson());
        Object obj = new ObjectMapper().convertValue(d1, Object.class);

        System.out.println(jsonData);
        System.out.println(obj);






    }
}
